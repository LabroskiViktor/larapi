<?php

namespace App;

use App\Product;
use App\User;
use Illuminate\Notifications\Notifiable;

class Seller extends User {

	use Notifiable;

	/**
	 * Seller-Product relationship
	 */
	public function products() {
		return $this->hasMany(Product::class);
	}
}
