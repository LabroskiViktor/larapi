<?php

namespace App;

use App\Category;
use App\Seller;
use App\Transaction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Product extends Model {
	use Notifiable;
	const AVAILABLE_PRODUCT = 'available';
	const UNAVAILABLE_PRODUCT = 'unavailable';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'description',
		'quantity',
		'status',
		'image',
		'seller_id',
	];

	public function isAvailable() {
		return $this->status == Product::AVAILABLE_PRODUCT;
	}

	/**
	 * Category-Product relationship
	 */
	public function categories() {
		return $this->belongsToMany(Category::class);
	}

	/**
	 * Product-Seller relationship
	 */
	public function seller() {
		return $this->belongsTo(Seller::class);
	}

	/**
	 * Product-Transaction relationship
	 */
	public function transactions() {
		return $this->hasMany(Transaction::class);
	}
}
