<?php

namespace App;

use App\Buyer;
use App\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Transaction extends Model {
	use Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'quantity',
		'buyer_id',
		'product_id',
	];

	/**
	 * Transaction-Buyer relationship
	 */
	public function buyer() {
		return $this->belongsTo(Buyer::class);
	}

	/**
	 * Transaction-Product relationship
	 */
	public function product() {
		return $this->belongsTo(Product::class);
	}
}
