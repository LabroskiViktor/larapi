<?php

namespace App;

use App\Transaction;
use Illuminate\Notifications\Notifiable;

class Buyer extends User {
	use Notifiable;
	/**
	 * Buyer-Transaction relationship
	 */
	public function transactions() {
		return $this->hasMany(Transaction::class);
	}
}
