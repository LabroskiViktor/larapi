<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
	use Notifiable;

	const VERIFIED_USER = '1';
	const UNVERIFIED_USER = '0';

	const ADMIN_USER = 'true';
	const REGULAR_USER = 'false';

	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
		'verified',
		'verification_token',
		'admin',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
		'verification_token',
	];

	/**
	 * mutator method
	 * @return string $name
	 * @param string $name
	 */
	public function setNameAttribute($name) {
		$this->attributes['name'] = strtolower($name);
	}

	/**
	 * accessor method
	 * @param  string $name
	 * @return string $name
	 */
	public function getNameAttribute($name) {
		return ucwords($name);
	}

	/**
	 * mutator method
	 * @return string $email
	 * @param string $email
	 */
	public function setEmailAttribute($email) {
		$this->attributes['email'] = strtolower($email);
	}

	/**
	 * check if user is verified
	 * @return boolean
	 */
	public function isVerified() {
		return $this->verified == User::VERIFIED_USER;
	}

	/**
	 * Check if user is admin
	 * @return boolean
	 */
	public function isAdmin() {
		return $this->admin == User::ADMIN_USER;
	}

	/**
	 * generateVerificationCode
	 * @return string
	 */
	public static function generateVerificationCode() {
		return str_random(40);
	}
}
